import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
import mind from '@/../vendor/mind-core-web'

Vue.use(mind)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
