module.exports = {
	publicPath: './',
	devServer: {
		disableHostCheck: true,
		https: true,
		port: 8080,
		proxy: {
			'/api': {
				target: process.env.API_PROTOCOL + '://' + process.env.API_HOST + ':' + process.env.API_PORT,
				secure: false,
				pathRewrite: { '^/api/[^/]+/': '' } 
			},
		}
	},
	configureWebpack: {
		plugins: [
		]
	}
}